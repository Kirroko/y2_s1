#include "SceneAsteroid.h"
#include "GL\glew.h"
#include "Application.h"
#include <sstream>

SceneAsteroid::SceneAsteroid()
{
}

SceneAsteroid::~SceneAsteroid()
{
}

void SceneAsteroid::Init()
{
	SceneBase::Init();

	// Others
	activeObject = 1;
	waveTimer = 30.f;
	wave = 0;
	wavePassed = false;
	m_enemySpawnRate = 1.f;
	m_enemyCount = 0;
	//Physics code here
	m_speed = 1.f;
	buddyMissile = 10.f;
	missileCount = MISSILE_COUNT;
	Math::InitRNG();

	// ROTATE_ANGLE = 90.f;
	angle = 0.0f;
	//Exercise 2a: Construct 100 GameObject with type GO_ASTEROID and add into m_goList
	int PRESPAWNOBJECT = 20;
	for (int i = 0; i < PRESPAWNOBJECT; ++i)
	{
		m_goList.push_back(new GameObject(GameObject::GO_ASTEROID));
	}
	//Exercise 2b: Initialize m_lives and m_score
	m_lives = 3;
	m_score = 0;
	enemyBulletCount = 0;
	enemyBulletCooldown = 0;
	bulletCooldown = 0;
	//Exercise 2c: Construct m_ship, set active, type, scale and pos

	//Calculating aspect ratio
	m_worldHeight = 100.f;
	m_worldWidth = m_worldHeight * (float)Application::GetWindowWidth() / Application::GetWindowHeight();

	m_ship = new GameObject(GameObject::GO_SHIP);
	m_ship->type = GameObject::GO_SHIP;
	m_ship->pos.Set(m_worldWidth * 0.5f, m_worldHeight * 0.5f, 0.f); // Divide is EXPENSEIVE
	m_ship->scale.Set(2.f, 2.f, 2.f);
	m_ship->momentOfInertia = m_ship->mass * (m_ship->scale.x * m_ship->scale.x);
	m_ship->angularVelocity = 0.f;
	m_ship->active = true;

	//m_goList[0]->pos.Set(Math::RandFloatMinMax(0.f, m_worldWidth), Math::RandFloatMinMax(0.f, m_worldHeight), 0);
	m_goList[0]->pos.Set(m_ship->pos.x - 5, m_ship->pos.y - 5, 0);
	m_goList[0]->dir = m_ship->dir;
	m_goList[0]->vel = m_ship->dir * m_speed;
	m_goList[0]->type = GameObject::GO_BUDDY;
	m_goList[0]->active = true;

	selectionID = 1;
}

GameObject* SceneAsteroid::FetchGO()
{
	//Exercise 3a: Fetch a game object from m_goList and return it
	for (auto it = m_goList.begin(); it != m_goList.end(); ++it)
	{
		GameObject *go = (GameObject *)* it;
		if (!go->active)
		{
			return go;
		}
	}
	for (int i = 0; i < 10; ++i)
	{
		m_goList.push_back(new GameObject(GameObject::GO_ASTEROID));
	}
	return m_goList.back();
}

GameObject * SceneAsteroid::SearchForTarget(const Vector3 & position)
{
	GameObject* target = nullptr;
	float dist = -1.f;

	for (auto go : m_goList)
	{
		if (!go->active || go->type != GameObject::GO_ASTEROID) continue;

		const float d = (go->pos - position).LengthSquared();
		if (d < dist || dist == -1)
		{
			target = go;
			dist = d;
		}
	}

	return target;
}

Vector3 SceneAsteroid::CalcDist(GameObject * target, GameObject * current)
{
	return target->pos - current->pos;
}

Vector3 SceneAsteroid::vectorLerp(Vector3 current, Vector3 target, float t)
{
	Vector3 r;
	float t_ = 1 - t;
	r = (t_ * current) + (t * target);
	return r;
}

float SceneAsteroid::Lerp(float angle, float targetAngle, float t)
{
	float r;
	float t_ = 1 - t;
	r = t_ * angle + t * targetAngle;
	return r;
}

float SceneAsteroid::SLerp(float angle, float targetAngle, float t)
{
	float r;
	float t_ = 1 - t;
	float Wa, Wb;
	float theta = acos(angle * targetAngle);
	float sn = sin(theta);
	Wa = sin(t_*theta) / sn;
	Wb = sin(t*theta) / sn;
	r = Wa * angle + Wb * targetAngle;
	return r;
}

void SceneAsteroid::Update(double dt)
{
	if (CURRENT_STATE == GAMEPLAY)
		waveTimer = Math::Max(0.f, waveTimer - static_cast<float>(dt));
	// calculate active object section
	activeObject = 1; // ship is always an active object
	for (auto it = m_goList.begin(); it != m_goList.end(); ++it) // recalculate the amount of active object
	{
		GameObject *go = (GameObject *)* it;
		if (go->active)
			activeObject++;
	}
	SceneBase::Update(dt);
	Vector3 sideForce;
	sideForce.Set(0.f, 15.f, 0.f);

	static bool keyDown = false;
	if (Application::IsKeyPressed(VK_DOWN) && CURRENT_STATE == MAINMENU)
	{
		if (!keyDown)
		{
			if (selectionID == 1) // start
			{
				selectionID++;
				start = Color(1, 1, 1);
				end = darkGreen;
			}
			else if (selectionID == 2) // exit
			{
				selectionID--;
				start = darkGreen;
				end = Color(1, 1, 1);
			}
			keyDown = true;
		}
	}
	else
		keyDown = false;

	static bool keyUp = false;
	if (Application::IsKeyPressed(VK_UP) && CURRENT_STATE == MAINMENU)
	{
		if (!keyUp)
		{
			if (selectionID == 1) // start
			{
				selectionID++;
				start = Color(1, 1, 1);
				end = darkGreen;
			}
			else if (selectionID == 2) // exit
			{
				selectionID--;
				start = darkGreen;
				end = Color(1, 1, 1);
			}
			keyUp = true;
		}
	}
	else
		keyUp = false;

	static bool keyEnter = false;
	if (Application::IsKeyPressed(VK_RETURN) && CURRENT_STATE == MAINMENU)
	{
		if (!keyEnter)
		{
			if (selectionID == 1)
				CURRENT_STATE = GAMEPLAY;
			else if (selectionID == 2)
			{
				Exit();
				exit(0);
			}
		}
	}

	if (Application::IsKeyPressed('9'))
	{
		m_speed = Math::Max(0.f, m_speed - 0.1f);
	}
	if (Application::IsKeyPressed('0'))
	{
		m_speed += 0.1f;
	}
	m_force.SetZero();
	//Exercise 6: set m_force values based on WASD
	if (Application::IsKeyPressed('W') && CURRENT_STATE == GAMEPLAY)
	{
		m_force += m_ship->dir * 100.0f;
	}
	if (Application::IsKeyPressed('A') && CURRENT_STATE == GAMEPLAY)
	{
		m_force += m_ship->dir * 5.0f;
		m_torque = Vector3(1.f, -1.f, 0.f).Cross(sideForce);
	}
	if (Application::IsKeyPressed('S') && CURRENT_STATE == GAMEPLAY)
	{
		m_force += m_ship->dir * -100.f;
	}
	if (Application::IsKeyPressed('D') && CURRENT_STATE == GAMEPLAY)
	{
		m_force += m_ship->dir * 5.0f;
		m_torque = Vector3(-1.f, -1.f, 0.f).Cross(sideForce);
	}
	if (Application::IsKeyPressed(VK_SPACE) && bulletCooldown <= 0 && CURRENT_STATE == GAMEPLAY)
	{
		//Exercise 15: limit the spawn rate of bullets
		GameObject * go = FetchGO();
		go->type = GameObject::GO_BULLET;
		//go->vel = m_ship->vel;
		go->pos = m_ship->pos;
		go->scale.Set(0.2f, 0.2f, 0.2f);
		if (!m_ship->vel.IsZero())
			go->vel = m_ship->dir * BULLET_SPEED;
		else
			go->vel.Set(BULLET_SPEED, 0, 0);
		go->active = true;

		bulletCooldown = 1.f;
	}
	bulletCooldown -= 3.5f * (float)dt;
	if (Application::IsKeyPressed('E') && missleCooldown <= 0 && CURRENT_STATE == GAMEPLAY && missileCount > 0)
	{
		GameObject * go = FetchGO();
		go->type = GameObject::GO_MISSILE;
		go->scale.Set(2.f, 2.f, 2.f);
		go->pos = m_ship->pos;
		go->dir = m_ship->dir;
		go->vel = m_ship->dir * MISSILE_SPEED;
		go->active = true;

		//GameObject* target = SearchForTarget(m_ship->pos);
		//if (target != nullptr) m_targetList.push_back(target);

		missleCooldown = 3.f;
		missileCount--;
	}
	missleCooldown -= 1.5f * (float)dt;

	if (buddyMissile <= 0 && CURRENT_STATE == GAMEPLAY)
	{
		GameObject * go = FetchGO();
		go->type = GameObject::GO_MISSILE;
		go->scale.Set(2.f, 2.f, 2.f);
		go->pos = m_goList[0]->pos;
		go->dir = m_goList[0]->dir;
		go->vel = m_goList[0]->dir * MISSILE_SPEED;
		go->active = true;

		buddyMissile = 10.f;
	}
	if (CURRENT_STATE == GAMEPLAY)
		buddyMissile -= (float)dt;

	//Exercise 8: use 2 keys to increase and decrease mass of ship
	if (Application::IsKeyPressed(VK_OEM_PLUS))
		m_ship->mass = Math::Min(100.f, m_ship->mass + 1.f * static_cast<float>(dt));
	if (Application::IsKeyPressed(VK_OEM_MINUS))
		m_ship->mass = Math::Max(0.1f, m_ship->mass - 1.f * static_cast<float>(dt));

	static const int SPAWNABLE_ASTEROID = 30;
	static const int SPAWNABLE_ENEMY = 15;
	static float PAUSE = 3.f;
	if (CURRENT_STATE == GAMEPLAY)
	{
		switch (wave)
		{
		case 0:
			PAUSE = Math::Max(0.f, PAUSE - static_cast<float>(dt));
			if (PAUSE <= 0.f)
				wave = 1;
			break;
		case 1:
			if (!wavePassed)
			{
				for (int i = 0; i < SPAWNABLE_ASTEROID; ++i)
				{
					GameObject * go = FetchGO();
					go->active = true;
					go->type = GameObject::GO_ASTEROID;
					go->scale.Set(1.f, 1.f, 1.f);
					go->pos.Set(static_cast<float>(Math::RandIntMinMax(0, static_cast<int>(m_worldWidth))),
						static_cast<float>(Math::RandIntMinMax(0, static_cast<int>(m_worldHeight))),
						0.0f);
					go->vel.Set(static_cast<float>(Math::RandIntMinMax(-10, 10)),
						static_cast<float>(Math::RandIntMinMax(-10, 10)),
						0.0f);
				}
				wavePassed = true;
			}
			if (waveTimer <= 0.f)
			{
				m_lives += 2;
				missileCount += 4;
				wave = 2;
				waveTimer = 30.f;
				wavePassed = false;
			}
			break;
		case 2:
			if (!wavePassed)
			{
				for (int i = 0; i < SPAWNABLE_ASTEROID; ++i)
				{
					GameObject * go = FetchGO();
					go->active = true;
					go->type = GameObject::GO_ASTEROID;
					go->scale.Set(1.f, 1.f, 1.f);
					go->pos.Set(static_cast<float>(Math::RandIntMinMax(0, static_cast<int>(m_worldWidth))),
						static_cast<float>(Math::RandIntMinMax(0, static_cast<int>(m_worldHeight))),
						0.0f);
					go->vel.Set(static_cast<float>(Math::RandIntMinMax(-15, 15)),
						static_cast<float>(Math::RandIntMinMax(-15, 15)),
						0.0f);
				}
				wavePassed = true;
			}
			if (m_enemyCount < 5 && m_enemySpawnRate >= 5.f)
			{
				m_enemySpawnRate = 0.f;
				GameObject * go = FetchGO();
				go->type = GameObject::GO_ENEMY;
				go->pos.Set(static_cast<float>(Math::RandIntMinMax(0, static_cast<int>(m_worldWidth))),
					static_cast<float>(Math::RandIntMinMax(0, static_cast<int>(m_worldHeight))), 0.0f);
				go->vel.Set(static_cast<float>(Math::RandIntMinMax(-5, 5)), static_cast<float>(Math::RandIntMinMax(-5, 5)), 0.0f);
				go->active = true;
				go->scale.Set(1, 1, 1);
				m_enemyCount++;
			}
			if (m_enemySpawnRate <= 5.f)
				m_enemySpawnRate += 3.5f * static_cast<float>(dt);
			if (waveTimer <= 0.f)
			{
				m_lives += 2;
				missileCount += 4;
				wave = 3;
				waveTimer = 30.f;
				wavePassed = false;
			}
			break;
		case 3:
			if (!wavePassed)
			{
				m_goList[1]->type = GameObject::GO_BLACKHOLE;
				m_goList[1]->active = true;
				m_goList[1]->pos.Set(m_worldWidth * 0.5f + 50.f, m_worldHeight * 0.5f + 25.f);
				m_goList[1]->scale.Set(5.f, 5.f, 1.f);
				m_goList[1]->mass = 5.f;

				for (int i = 0; i < SPAWNABLE_ASTEROID; ++i)
				{
					GameObject * go = FetchGO();
					go->active = true;
					go->type = GameObject::GO_ASTEROID;
					go->scale.Set(1.f, 1.f, 1.f);
					go->pos.Set(static_cast<float>(Math::RandIntMinMax(0, static_cast<int>(m_worldWidth))),
						static_cast<float>(Math::RandIntMinMax(0, static_cast<int>(m_worldHeight))),
						0.0f);
					go->vel.Set(static_cast<float>(Math::RandIntMinMax(-15, 15)),
						static_cast<float>(Math::RandIntMinMax(-15, 15)),
						0.0f);
				}

				wavePassed = true;
			}
			if (m_enemyCount < 5 && m_enemySpawnRate >= 5.f)
			{
				m_enemySpawnRate = 0.f;
				GameObject * go = FetchGO();
				go->type = GameObject::GO_ENEMY;
				go->pos.Set(static_cast<float>(Math::RandIntMinMax(0, static_cast<int>(m_worldWidth))),
					static_cast<float>(Math::RandIntMinMax(0, static_cast<int>(m_worldHeight))), 0.0f);
				go->vel.Set(static_cast<float>(Math::RandIntMinMax(-5, 5)), static_cast<float>(Math::RandIntMinMax(-5, 5)), 0.0f);
				go->scale.Set(1, 1, 1);
				m_enemyCount++;
			}
			if (m_enemySpawnRate <= 5.f)
				m_enemySpawnRate += 3.5f * static_cast<float>(dt);

			if (waveTimer <= 0.f)
			{
				for (auto it = m_goList.begin(); it != m_goList.end(); ++it)
				{
					GameObject *go = (GameObject *)* it;
					go->active = false;
				}
				m_ship->active = false;
				CURRENT_STATE = GAMEOVER;
			}
			break;
		default:
			break;
		}
	}
	//Exercise 11: use a key to spawn some asteroids
	/*static bool spawned_buddy = false;
	if (Application::IsKeyPressed('Q') && CURRENT_STATE == GAMEPLAY)
	{
		if (!spawned_buddy)
		{
			std::cout << "Q DOWN" << std::endl;
			GameObject* go = FetchGO();
			go->pos.Set(Math::RandFloatMinMax(0.f, m_worldWidth), Math::RandFloatMinMax(0.f, m_worldHeight), 0);
			go->dir = m_ship->dir;
			go->vel = m_ship->dir * m_speed;
			go->type = GameObject::GO_BUDDY;
			go->active = true;
			spawned_buddy = true;
		}
	}
	else
		spawned_buddy = false;*/
		/*static bool spawned_asteroid = false;
		if (Application::IsKeyPressed('V') && CURRENT_STATE == GAMEPLAY)
		{
		}
		else
			spawned_asteroid = false;*/
			/*static bool spawned_enemy = false;
			if (Application::IsKeyPressed('R') && CURRENT_STATE == GAMEPLAY)
			{
				if (!spawned_enemy)
				{
					spawned_enemy = true;
					for (int i = 0; i < 1; ++i)
					{
						GameObject * go = FetchGO();
						if (go != NULL)
						{
							go->active = true;
							go->type = GameObject::GO_ENEMY;
							go->scale.Set(1.0f, 1.0f, 1.0f);
							go->pos.Set(static_cast<float>(Math::RandIntMinMax(0, static_cast<int>(m_worldWidth))),
								static_cast<float>(Math::RandIntMinMax(0, static_cast<int>(m_worldHeight))), 0.0f);
							go->vel.Set(static_cast<float>(Math::RandIntMinMax(-5, 5)), static_cast<float>(Math::RandIntMinMax(-5, 5)), 0.0f);
						}
					}
				}
			}
			else
				spawned_enemy = false;*/
				//static bool spawn_blackHole = false;
				//if (Application::IsKeyPressed('O') && CURRENT_STATE == GAMEPLAY)
				//{
				//	if (!spawn_blackHole)
				//	{
				//		// Blackhole setup
				//		m_goList[0]->type = GameObject::GO_BLACKHOLE;
				//		m_goList[0]->active = true;
				//		m_goList[0]->pos.Set(m_worldWidth * 0.5f + 50.f, m_worldHeight * 0.5f + 25.f);
				//		m_goList[0]->scale.Set(5.f, 5.f, 1.f);
				//		m_goList[0]->mass = 5.f;
				//		spawn_blackHole = true;
				//	}
				//}
				//else
				//	spawn_blackHole = false;

				//Mouse Section
	static bool bLButtonState = false;
	if (!bLButtonState && Application::IsMousePressed(0))
	{
		bLButtonState = true;
		std::cout << "LBUTTON DOWN" << std::endl;
	}
	else if (bLButtonState && !Application::IsMousePressed(0))
	{
		bLButtonState = false;
		std::cout << "LBUTTON UP" << std::endl;
	}
	static bool bRButtonState = false;
	if (!bRButtonState && Application::IsMousePressed(1))
	{
		bRButtonState = true;
		std::cout << "RBUTTON DOWN" << std::endl;
	}
	else if (bRButtonState && !Application::IsMousePressed(1))
	{
		bRButtonState = false;
		std::cout << "RBUTTON UP" << std::endl;
	}

	//Physics Simulation Section
	m_ship->momentOfInertia = m_ship->mass * (m_ship->scale.x * m_ship->scale.x);
	m_ship->angularVelocity += (m_torque.z / m_ship->momentOfInertia) * static_cast<float>(dt);
	m_ship->angularVelocity = Math::Clamp(m_ship->angularVelocity, -10.0f, 10.0f);
	angle += m_ship->angularVelocity * static_cast<float>(dt);
	m_ship->dir = Vector3(cosf(angle), sinf(angle), 0.f).Normalized();
	m_torque.SetZero();

	//Exercise 7: Update ship's velocity based on m_force
	// F = ma >> A = f / m
	Vector3 acceleration = m_force * (1.0f / m_ship->mass); // since cant divide, multiply by reciprocal
	m_ship->vel += acceleration * (m_speed * static_cast<float>(dt));
	m_ship->vel.x = Math::Clamp(m_ship->vel.x, -10.f, 10.f);
	m_ship->vel.y = Math::Clamp(m_ship->vel.y, -10.f, 10.f);
	m_ship->pos += m_ship->vel * (m_speed * static_cast<float>(dt));

	//Exercise 9: wrap ship position if it leaves
	m_ship->pos.Set(Math::Wrap(m_ship->pos.x, 0.f, m_worldWidth), Math::Wrap(m_ship->pos.y, 0.f, m_worldHeight), 0);

	for (std::vector<GameObject *>::iterator it = m_goList.begin(); it != m_goList.end(); ++it)
	{
		GameObject *go = (GameObject *)*it;
		if (go->active)
		{
			//Exercise 12: handle collision between GO_SHIP and GO_ASTEROID using simple distance-based check
			if (go->type == GameObject::GO_ASTEROID)
			{
				go->pos += go->vel * m_speed * static_cast<float>(dt);
				float combindedRadii = go->scale.x + m_ship->scale.x;
				if ((go->pos - m_ship->pos).LengthSquared() < combindedRadii * combindedRadii)
				{
					m_ship->pos.Set(m_worldWidth / 2, m_worldHeight / 2, 0);
					m_ship->vel.SetZero();
					m_ship->angularVelocity = 0.f;
					--m_lives;
					if (m_lives == 0)
						Restart();
					go->active = false;
					break;
				}

				//Exercise 13: asteroids should wrap around the screen like the ship
				go->pos.Set(Math::Wrap(go->pos.x, 0.f, m_worldWidth), Math::Wrap(go->pos.y, 0.f, m_worldHeight), 0);
			}
			//Exercise 16: unspawn bullets when they leave screen
			if (go->type == GameObject::GO_BULLET)
			{
				go->pos += go->vel * static_cast<float>(dt);

				//Exercise 18: collision check between GO_BULLET and GO_ASTEROID
				for (std::vector<GameObject *>::iterator it = m_goList.begin(); it != m_goList.end(); ++it)
				{
					GameObject *gogo = (GameObject *)*it;
					if (!gogo->active)
						continue;
					if (gogo->type == GameObject::GO_ASTEROID)
					{
						float combindedRadii = go->scale.x + gogo->scale.x;
						if ((go->pos - gogo->pos).LengthSquared() < combindedRadii * combindedRadii)
						{
							go->active = false;
							gogo->active = false;
							m_score += 2;
							break;
						}
					}
					if (gogo->type == GameObject::GO_ENEMY)
					{
						float combindedRadii = go->scale.x + gogo->scale.x;
						if ((go->pos - gogo->pos).LengthSquared() < combindedRadii * combindedRadii)
						{
							go->active = false;
							gogo->active = false;
							m_score += 4;
							break;
						}
						m_enemyCount--;
					}
				}
				int POS_OFFSET = 1;
				if (go->pos.x <= 0 - POS_OFFSET)
					go->active = false;
				else if (go->pos.x >= m_worldWidth + POS_OFFSET)
					go->active = false;
				else if (go->pos.y <= 0 - POS_OFFSET)
					go->active = false;
				else if (go->pos.y >= m_worldHeight + POS_OFFSET)
					go->active = false;
			}

			if (go->type == GameObject::GO_ENEMY)
			{
				float smooth = 1.f;
				go->vel = (m_ship->pos - go->pos).Normalized();
				go->pos += go->vel * MAX_SPEED * static_cast<float>(dt);
				Vector3 targetDir = (m_ship->pos - go->pos).Normalized();
				go->dir = vectorLerp(go->dir, targetDir, 10.f * smooth * static_cast<float>(dt));
				go->angle = atan2(go->dir.y, go->dir.x);

				// GO_ENEMY shooting of GO_ENEMY_BULLET
				enemyBulletCooldown -= 1.f * static_cast<float>(dt);
				if (enemyBulletCount < 100 && enemyBulletCooldown <= 0)
				{
					GameObject *temp = FetchGO();
					temp->type = GameObject::GO_ENEMY_BULLET;
					temp->scale.Set(0.2f, 0.2f, 0.2f);
					temp->pos = go->pos;
					temp->vel = go->dir.Normalized() * BULLET_SPEED;
					temp->active = true;
					enemyBulletCount++;
					enemyBulletCooldown = 1.f;
				}
				float combindedRadii = go->scale.x + m_ship->scale.x;
				if ((go->pos - m_ship->pos).LengthSquared() < combindedRadii * combindedRadii)
				{
					m_ship->pos.Set(m_worldWidth / 2, m_worldHeight / 2, 0);
					m_ship->vel.SetZero();
					m_ship->angularVelocity = 0.f;
					--m_lives;
					if (m_lives == 0)
						Restart();
					go->active = false;
				}
				for (auto it = m_goList.begin(); it != m_goList.end(); ++it)
				{
					GameObject *gogo = (GameObject *)* it;
					if (gogo->active && gogo->type == GameObject::GO_ASTEROID)
					{
						float combindedRadii = go->scale.x + gogo->scale.x;
						if ((go->pos - gogo->pos).LengthSquared() < combindedRadii * combindedRadii)
						{
							go->active = false;
						}
					}
				}
			}

			if (go->type == GameObject::GO_ENEMY_BULLET)
			{
				go->pos += go->vel * static_cast<float>(dt);
				if (go->active && go->pos.x > m_worldWidth || go->pos.x < 0 || go->pos.y >
					m_worldHeight || go->pos.y < 0)
				{
					go->active = false;
					enemyBulletCount--;
				}
				float combindedRadii = go->scale.x + m_ship->scale.x;
				if ((go->pos - m_ship->pos).LengthSquared() < combindedRadii * combindedRadii)
				{
					m_ship->pos.Set(m_worldWidth / 2, m_worldHeight / 2, 0);
					m_ship->vel.SetZero();
					m_ship->angularVelocity = 0.f;
					--m_lives;
					if (m_lives == 0)
						Restart();
					go->active = false;
					break;
				}
			}

			if (go->type == GameObject::GO_MISSILE)
			{
				Vector3 displacement = Vector3(FLT_MAX / 2, FLT_MAX / 2, FLT_MAX / 2);
				GameObject* nearestObj = nullptr;
				for (std::vector<GameObject*>::iterator it2 = m_goList.begin(); it2 != m_goList.end(); ++it2)
				{
					// Missile tracking logic
					GameObject *go2 = (GameObject *)*it2;
					if (!go2->active)
						continue;
					if ((go2->pos - go->pos).LengthSquared() < displacement.LengthSquared()
						&& go2->type != GameObject::GO_BULLET
						&& go2->type != GameObject::GO_BLACKHOLE
						&& go2->type != GameObject::GO_MISSILE
						&& go2->type != GameObject::GO_BUDDY)
					{
						displacement = go2->pos - go->pos;
						nearestObj = go2;
						if ((go2->pos - go->pos).LengthSquared() < 2.f*2.f)
						{
							go2->active = go->active = false;
							m_score++;
						}
					}
					Vector3 force;
					// missile phyics
					if (nearestObj != nullptr)
					{
						if (go->dir.Dot(displacement) > 0)
						{
							float weight = 0.001f;
							go->dir = (1 - weight) * go->dir + weight * displacement.Normalized();
						}
						go->angle = atan2(go->dir.y, go->dir.x);
						go->vel = go->dir * MISSILE_SPEED;
						go->pos += go->vel * static_cast<float>(dt);
					}
					else
					{
						go->angle = atan2(go->dir.y, go->dir.x);
						go->vel = go->dir * MISSILE_SPEED;
						go->pos += go->vel * static_cast<float>(dt);
					}
				}
				if (go->active && go->pos.x > m_worldWidth || go->pos.x < 0 || go->pos.y >
					m_worldHeight || go->pos.y < 0)
				{
					go->active = false;
				}
			}

			if (go->type == GameObject::GO_BLACKHOLE)
			{
				float r = Math::Min(CalcDist(go, m_ship).LengthSquared(), 500.f);
				float attractionForce = (go->mass * m_ship->mass / r) * 10.f;

				if (CalcDist(go, m_ship).LengthSquared() < (go->scale.x * go->scale.x) * 10.f)
					m_ship->vel += CalcDist(go, m_ship).Normalized() * attractionForce;

				// When player gets sucked into the blackhole.
				if (CalcDist(go, m_ship).LengthSquared() < go->scale.x * go->scale.x)
				{
					m_ship->pos.Set(m_worldWidth / 2.f, m_worldHeight / 2.f, 0.f); // reset to original position
					m_ship->vel.SetZero();
					m_lives = 3;
					m_score = 0;
				}
				for (int i = 1; i < m_goList.size(); ++i)
				{
					GameObject* go2 = m_goList[i];
					if (go2->active && go2->type != go->type)
					{
						float r2 = (go->pos - go2->pos).LengthSquared();
						float attractionForce = (go->mass / r2) * 40.f;

						if ((go->pos - go2->pos).LengthSquared() < 100.f * 80.f)
							go2->vel += (go->pos - go2->pos).Normalized() * attractionForce;

						if ((go->pos - go2->pos).LengthSquared() < go->scale.x * go->scale.x)
							go2->active = false;
					}
				}
			}

			if (go->type == GameObject::GO_BUDDY)
			{
				Vector3 displacement = Vector3(FLT_MAX / 2, FLT_MAX / 2, FLT_MAX / 2);
				displacement = m_ship->pos - go->pos;
				float weight = 0.5f;
				go->dir = (1 - weight) * go->dir + weight * displacement.Normalized();
				go->angle = atan2(go->dir.y, go->dir.x);
				go->vel = go->dir * 10.f;
				float offset = 10.f;
				if (displacement.LengthSquared() >= offset)
					go->pos += go->vel * static_cast<float>(dt);
			}
		}
	}
}

void SceneAsteroid::RenderGO(GameObject *go)
{
	// ROTATE_ANGLE = Math::RadianToDegree(atan2f(m_ship->vel.y, m_ship->vel.x));
	switch (go->type)
	{
	case GameObject::GO_SHIP:
		//Exercise 4a: render a sphere with radius 1
		modelStack.PushMatrix();
		modelStack.Translate(m_ship->pos.x, m_ship->pos.y, m_ship->pos.z);
		// modelStack.Rotate(-90, 0, 0, 1);
		modelStack.Rotate(Math::RadianToDegree(angle), 0, 0, 1);
		modelStack.Scale(m_ship->scale.x, m_ship->scale.y, m_ship->scale.z);
		RenderMesh(meshList[GEO_SHIP], false);
		modelStack.PopMatrix();
		//Exercise 17a: render a ship texture or 3D ship model
		//Exercise 17b:	re-orientate the ship with velocity
		break;
	case GameObject::GO_ASTEROID:
		//Exercise 4b: render a cube with length 2
		modelStack.PushMatrix();
		modelStack.Translate(go->pos.x, go->pos.y, go->pos.z);
		modelStack.Scale(go->scale.x, go->scale.y, go->scale.z);
		RenderMesh(meshList[GEO_ASTEROID], false);
		modelStack.PopMatrix();
		break;
	case GameObject::GO_BULLET:
		//Exercise 4c: render a sphere with radius
		modelStack.PushMatrix();
		modelStack.Translate(go->pos.x, go->pos.y, go->pos.z);
		modelStack.Scale(go->scale.x, go->scale.y, go->scale.z);
		RenderMesh(meshList[GEO_BALL], false);
		modelStack.PopMatrix();
		break;
	case GameObject::GO_ENEMY:
		modelStack.PushMatrix();
		modelStack.Translate(go->pos.x, go->pos.y, go->pos.z);
		modelStack.Rotate(Math::RadianToDegree(go->angle), 0, 0, 1);
		modelStack.Scale(go->scale.x, go->scale.y, go->scale.z);
		RenderMesh(meshList[GEO_ENEMYSHIP], false);
		modelStack.PopMatrix();
		break;
	case GameObject::GO_ENEMY_BULLET:
		modelStack.PushMatrix();
		modelStack.Translate(go->pos.x, go->pos.y, go->pos.z);
		modelStack.Scale(go->scale.x, go->scale.y, go->scale.z);
		RenderMesh(meshList[GEO_BALL], false);
		modelStack.PopMatrix();
		break;
	case GameObject::GO_MISSILE:
		modelStack.PushMatrix();
		modelStack.Translate(go->pos.x, go->pos.y, go->pos.z);
		modelStack.Rotate(Math::RadianToDegree(go->angle), 0, 0, 1);
		modelStack.Scale(go->scale.x, go->scale.y, go->scale.z);
		RenderMesh(meshList[GEO_MISSILE], false);
		modelStack.PopMatrix();
		break;
	case GameObject::GO_BLACKHOLE:
		modelStack.PushMatrix();
		modelStack.Translate(go->pos.x, go->pos.y, go->pos.z);
		modelStack.Scale(go->scale.x, go->scale.y, go->scale.z);
		RenderMesh(meshList[GEO_BLACKHOLE], false);
		modelStack.PopMatrix();
		break;
	case GameObject::GO_BUDDY:
		modelStack.PushMatrix();
		modelStack.Translate(go->pos.x, go->pos.y, go->pos.z);
		modelStack.Rotate(Math::RadianToDegree(angle), 0, 0, 1);
		modelStack.Scale(go->scale.x, go->scale.y, go->scale.z);
		RenderMesh(meshList[GEO_SHIP], false);
		modelStack.PopMatrix();
		break;
	}
}

void SceneAsteroid::RenderMenu()
{
	std::ostringstream ss;

	ss << "ASTEROID";
	RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), Color(1, 1, 1), 4, 25, 35);
	ss.str("");

	ss << "Start";
	RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), start, 3, 33, 26);
	ss.str("");

	ss << "exit";
	RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), end, 3, 34.5, 23);
	ss.str("");
}

void SceneAsteroid::RenderGameOver()
{
	std::ostringstream ss;

	ss << "GAME OVER";
	RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), Color(1, 1, 1), 4, 25, 35);
	ss.str("");

	ss << "Score : " << m_score;
	RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), Color(1, 1, 1), 3, 31, 23);
	ss.str("");

	static float delay = 3.0f;
	if (delay < 0)
	{
		Exit();
		exit(0);
	}
	delay -= 0.1f;
}

void SceneAsteroid::Restart()
{
	//for (std::vector<GameObject *>::iterator it = m_goList.begin(); it != m_goList.end(); ++it) // reset all object inside m_goList to non-active
	//{
	//	GameObject *go = (GameObject *)*it;
	//	if (go->active)
	//		go->active = false;
	//}
	//m_ship->pos.Set(m_worldWidth / 2, m_worldHeight / 2, 0); // reset the ship to the centre
	//m_ship->momentOfInertia = m_ship->mass * (m_ship->scale.x * m_ship->scale.x); // reset the ship from spinning
	//m_ship->angularVelocity = 0.f; // reset the ship from spinning
	//m_lives = 3; // reset the lives to 3;
	for (auto it = m_goList.begin(); it != m_goList.end(); ++it)
	{
		GameObject *go = (GameObject *)* it;
		go->active = false;
	}
	m_ship->active = false;
	CURRENT_STATE = GAMEOVER;
}

void SceneAsteroid::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Projection matrix : Orthographic Projection
	Mtx44 projection;
	projection.SetToOrtho(0, m_worldWidth, 0, m_worldHeight, -10, 10);
	projectionStack.LoadMatrix(projection);

	// Camera matrix
	viewStack.LoadIdentity();
	viewStack.LookAt(
		camera.position.x, camera.position.y, camera.position.z,
		camera.target.x, camera.target.y, camera.target.z,
		camera.up.x, camera.up.y, camera.up.z
	);
	// Model matrix : an identity matrix (model will be at the origin)
	modelStack.LoadIdentity();

	RenderMesh(meshList[GEO_AXES], false);

	// Background
	float scaleSize = 500;
	modelStack.PushMatrix();
	modelStack.Translate(0, 0, -1);
	modelStack.Scale(scaleSize, scaleSize, scaleSize);
	RenderMesh(meshList[GEO_BACKGROUND], false);
	modelStack.PopMatrix();

	std::ostringstream ss;
	switch (CURRENT_STATE)
	{
	case SceneAsteroid::MAINMENU:
		RenderMenu();
		break;
	case SceneAsteroid::GAMEPLAY:
		for (std::vector<GameObject *>::iterator it = m_goList.begin(); it != m_goList.end(); ++it)
		{
			GameObject *go = (GameObject *)*it;
			if (go->active)
			{
				RenderGO(go);
			}
		}
		if (m_ship->active)
			RenderGO(m_ship);

		//On screen text

		//Exercise 5a: Render m_lives, m_score
		ss.precision(3);
		ss << "Enemies : " << m_enemyCount;
		RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), Color(1, 1, 1), 2, 0, 30);
		ss.str("");

		ss << "Missile : " << missileCount;
		RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), Color(1, 1, 1), 2, 0, 27);
		ss.str("");

		ss.precision(2);
		ss << "Wave : " << wave;
		RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), Color(1, 1, 1), 2, 0, 24);
		ss.str("");

		ss.precision(2);
		ss << "Wave Timer : " << waveTimer;
		RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), Color(1, 1, 1), 2, 0, 21);
		ss.str("");

		ss.precision(3);
		ss << "Active Object : " << activeObject;
		RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), Color(1, 1, 1), 2, 0, 18);
		ss.str("");

		ss << "Lives: " << m_lives;
		RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), Color(1, 1, 1), 2, 0, 53);
		ss.str("");

		//ss1.precision(3);
		ss << "Score: " << m_score;
		RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), Color(1, 1, 1), 2, 0, 50);
		ss.str("");
		//Exercise 5b: Render position, velocity & mass of ship
		//ss1.precision(3);
		ss << "Ship Pos: " << m_ship->pos;
		RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), Color(0, 1, 0), 2, 0, 12);
		ss.str("");
		//ss1.precision(3);
		ss << "Ship Vel: " << m_ship->vel;
		RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), Color(0, 1, 0), 2, 0, 9);
		ss.str("");
		//ss1.precision(3);
		ss << "Ship Mass: " << m_ship->mass;
		RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), Color(0, 1, 0), 2, 0, 6);
		ss.str("");

		ss.precision(3);
		ss << "Speed: " << m_speed;
		RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), Color(0, 1, 0), 2, 0, 3);
		ss.str("");

		ss.precision(5);
		ss << "FPS: " << fps;
		RenderTextOnScreen(meshList[GEO_TEXT], ss.str(), Color(0, 1, 0), 2, 0, 0);
		ss.str("");
		RenderTextOnScreen(meshList[GEO_TEXT], "Asteroid", Color(0, 1, 0), 2, 0, 56);
		break;
	case SceneAsteroid::GAMEOVER:
		RenderGameOver();
		break;
	default:
		break;
	}
}

void SceneAsteroid::Exit()
{
	//Cleanup GameObjects
	while (m_goList.size() > 0)
	{
		GameObject *go = m_goList.back();
		delete go;
		m_goList.pop_back();
	}
	if (m_ship)
	{
		delete m_ship;
		m_ship = NULL;
	}
	SceneBase::Exit();
}