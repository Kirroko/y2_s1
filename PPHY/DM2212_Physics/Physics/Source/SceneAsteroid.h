#ifndef SCENE_ASTEROID_H
#define SCENE_ASTEROID_H

#include "GameObject.h"
#include <vector>
#include "SceneBase.h"
#include "timer.h"



class SceneAsteroid : public SceneBase
{
	static const int MAX_SPEED = 10;
	static const int BULLET_SPEED = 50;
	static const int MISSILE_COUNT = 5;
	static const int MISSILE_SPEED = 2;
	static const int MISSILE_POWER = 1;
	static const int MISSILE_GRAVITY = 1;
	static const int MISSILE_ROTATION_FORCE = 2;


	enum GAME_STATE
	{
		MAINMENU,
		GAMEPLAY,
		GAMEOVER,
		TOTAL_COUNT
	};
	GAME_STATE CURRENT_STATE = MAINMENU;
public:
	SceneAsteroid();
	~SceneAsteroid();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

	void RenderGO(GameObject *go);
	void RenderMenu();
	void RenderGameOver();
	void Restart();

	GameObject* FetchGO();
	GameObject* SearchForTarget(const Vector3& position);
	Vector3 CalcDist(GameObject* target, GameObject* current);
	Vector3 vectorLerp(Vector3 current, Vector3 target, float t);
	float Lerp(float angle, float targetAngle, float t); // Linear Interpolation
	float SLerp(float angle, float targetAngle, float t); // Spherical Linear Interpolation
protected:
	// Menu
	Color lightPink = Color(0.7294f, 0.4549f, 0.5412f);
	Color darkGreen = Color(0, 153.f/255.f, 51.f/255.f);
	
	Color start = darkGreen;
	Color end = Color(1,1,1);
	int selectionID;
	
	//Physics
	std::vector<GameObject *> m_goList;
	std::vector<GameObject*> m_targetList;
	float m_speed;
	float m_worldWidth;
	float m_worldHeight;
	GameObject *m_ship;
	Vector3 m_force;
	Vector3 m_torque;
	unsigned m_missileUsed;
	int m_objectCount;
	int m_lives;
	int m_score;
	int missileCount;
	int enemyBulletCount;
	float enemyBulletCooldown;
	float bulletCooldown;
	float missleCooldown;
	float buddyMissile;
	float angle;
	StopWatch m_timer;
	int activeObject;
	int m_enemyCount;
	// wave
	float waveTimer;
	int wave;
	bool wavePassed;
	float m_enemySpawnRate;
};

#endif