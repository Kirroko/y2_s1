#include "SceneCollision.h"
#include "GL\glew.h"
#include "Application.h"
#include <sstream>

SceneCollision::SceneCollision()
{
}

SceneCollision::~SceneCollision()
{
}

void SceneCollision::Init()
{
	SceneBase::Init();

	//Physics code here
	m_speed = 1.f;

	Math::InitRNG();

	//Exercise 1: initialize m_objectCount
	m_objectCount = 0;
	static const int SPAWNABLE_OBJECT = 10;
	for (int i = 0; i < SPAWNABLE_OBJECT; ++i)
	{
		m_goList.push_back(new GameObject(GameObject::GO_BALL));
	}

	m_ghost = new GameObject(GameObject::GO_BALL);
}

GameObject* SceneCollision::FetchGO()
{
	//Exercise 2a: implement FetchGO()
	for (auto it = m_goList.begin(); it != m_goList.end(); ++it)
	{
		GameObject *go = (GameObject *)* it;
		if (!go->active)
		{
			go->active = true;
			++m_objectCount;
			return go;
		}
	}
	//Exercise 2b: increase object count every time an object is set to active
	static const int NEW_SPAWNABLE_OBJECT = 10;
	for (int i = 0; i < NEW_SPAWNABLE_OBJECT; ++i)
	{
		m_goList.push_back(new GameObject(GameObject::GO_BALL));
	}
	m_goList.back()->active = true;
	++m_objectCount;
	return m_goList.back();
}

void SceneCollision::Update(double dt)
{
	SceneBase::Update(dt);
	if (Application::IsKeyPressed('9'))
	{
		m_speed = Math::Max(0.f, m_speed - 0.1f);
	}
	if (Application::IsKeyPressed('0'))
	{
		m_speed += 0.1f;
	}

	//Mouse Section
	static bool bLButtonState = false;
	if (!bLButtonState && Application::IsMousePressed(0))
	{
		bLButtonState = true;
		std::cout << "LBUTTON DOWN" << std::endl;

		double x, y;
		Application::GetCursorPos(&x, &y);
		int w = Application::GetWindowWidth();
		int h = Application::GetWindowHeight();
		float worldX = x * m_worldWidth / w;
		float worldY = (h - y) * m_worldHeight / h;

		m_ghost->vel.SetZero();
		m_ghost->pos.Set(worldX, m_worldHeight * 0.5f, 0);
	}
	else if (bLButtonState && !Application::IsMousePressed(0))
	{
		bLButtonState = false;
		std::cout << "LBUTTON UP" << std::endl;

		//Exercise 6: spawn small GO_BALL
		double x, y;
		Application::GetCursorPos(&x, &y);
		int w = Application::GetWindowWidth();
		int h = Application::GetWindowHeight();
		float worldX = x * m_worldWidth / w;
		float worldY = (h - y) * m_worldHeight / h;

		GameObject* go = FetchGO();
		go->pos.Set(m_ghost->pos.x, m_ghost->pos.y, m_ghost->pos.z);
		go->vel = m_ghost->pos - Vector3(worldX, worldY, 0);
		go->vel.y = 0;
		go->active = true;

	}
	static bool bRButtonState = false;
	if (!bRButtonState && Application::IsMousePressed(1))
	{
		bRButtonState = true;
		std::cout << "RBUTTON DOWN" << std::endl;

		double x, y;
		Application::GetCursorPos(&x, &y);
		int w = Application::GetWindowWidth();
		int h = Application::GetWindowHeight();
		float worldX = x * m_worldWidth / w;
		float worldY = (h - y) * m_worldHeight / h;

		m_ghost->vel.SetZero();
		m_ghost->pos.Set(worldX, m_worldHeight * 0.5f, 0);
	}
	else if (bRButtonState && !Application::IsMousePressed(1))
	{
		bRButtonState = false;
		std::cout << "RBUTTON UP" << std::endl;

		//Exercise 10: spawn large GO_BALL
		double x, y;
		Application::GetCursorPos(&x, &y);
		int w = Application::GetWindowWidth();
		int h = Application::GetWindowHeight();
		float worldX = x * m_worldWidth / w;
		float worldY = (h - y) * m_worldHeight / h;
		
		GameObject *go = FetchGO();
		go->pos.Set(m_ghost->pos.x, m_ghost->pos.y, m_ghost->pos.z);
		go->vel = m_ghost->pos - Vector3(worldX, worldY, 0);
		go->vel.y = 0;
		go->scale.Set(1.5f, 1.5f, 1.5f);
		go->mass = go->scale.x * go->scale.y * go->scale.z;
	}

	//Physics Simulation Section
	for (std::vector<GameObject *>::iterator it = m_goList.begin(); it != m_goList.end(); ++it)
	{
		GameObject *go = (GameObject *)*it;
		if (go->active)
		{
			if (go->type == GameObject::GO_BALL)
			{
				//Exercise 7: handle out of bound game objects
				// Check the position
				if (go->pos.x < go->scale.x || go->pos.x > m_worldWidth - go->scale.x)
					go->vel.x = -go->vel.x;

				if (go->pos.y < go->scale.y || go->pos.y > m_worldHeight - go->scale.y)
					go->vel.y = -go->vel.y;

				if (go->pos.x < 0.f || go->pos.x > m_worldWidth || go->pos.y < 0.f || go->pos.y > m_worldHeight)
				{
					go->active = false;
					--m_objectCount;
				}

				//Exercise 8a: handle collision between GO_BALL and GO_BALL using velocity swap
				//Exercise 8b: store values in auditing variables
				//for (auto it2 = m_goList.begin(); it2 != m_goList.end(); ++it2)
				//for(int j = i + 1; j < m_goList.size(); ++j)
				for(std::vector<GameObject *>::iterator it2 = it + 1; it2 != m_goList.end(); ++it2)
				{
					GameObject *gogo = (GameObject *)*it2;
					// GameObject* gogo = m_goList[j];
					float combindedRadii = go->scale.x + gogo->scale.x;
					if (!gogo->active)
						continue;
					if ((go->pos - gogo->pos).LengthSquared() < combindedRadii * combindedRadii)
					{
						u1 = go->vel;
						u2 = gogo->vel;

						v1 = go->vel;
						v2 = gogo->vel;

						m1 = go->mass;
						m2 = gogo->mass;

						//Exercise 10: handle collision using momentum swap instead
						go->vel = u2 * (m2 / m1);
						gogo->vel = u1 * (m1 / m2);

						/*go->vel = u2;
						gogo->vel = u1;*/
						
					}
					
				}
				go->pos += go->vel * (m_speed * static_cast<float>(dt)); // Ball movement
			}


			//Exercise 12: improve inner loop to prevent double collision

			//Exercise 13: improve collision detection algorithm [solution to be given later]
		}
	}
}

void SceneCollision::RenderGO(GameObject *go)
{
	switch (go->type)
	{
	case GameObject::GO_BALL:
		//Exercise 4: render a sphere using scale and pos
		modelStack.PushMatrix();
		modelStack.Translate(go->pos.x, go->pos.y, go->pos.z);
		modelStack.Scale(go->scale.x, go->scale.y, go->scale.z);
		RenderMesh(meshList[GEO_BALL], false);
		modelStack.PopMatrix();
		//Exercise 11: think of a way to give balls different colors
		break;
	}
}

void SceneCollision::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Calculating aspect ratio
	m_worldHeight = 100.f;
	m_worldWidth = m_worldHeight * (float)Application::GetWindowWidth() / Application::GetWindowHeight();

	// Projection matrix : Orthographic Projection
	Mtx44 projection;
	projection.SetToOrtho(0, m_worldWidth, 0, m_worldHeight, -10, 10);
	projectionStack.LoadMatrix(projection);

	// Camera matrix
	viewStack.LoadIdentity();
	viewStack.LookAt(
		camera.position.x, camera.position.y, camera.position.z,
		camera.target.x, camera.target.y, camera.target.z,
		camera.up.x, camera.up.y, camera.up.z
	);
	// Model matrix : an identity matrix (model will be at the origin)
	modelStack.LoadIdentity();

	RenderMesh(meshList[GEO_AXES], false);

	for (std::vector<GameObject *>::iterator it = m_goList.begin(); it != m_goList.end(); ++it)
	{
		GameObject *go = (GameObject *)*it;
		if (go->active)
		{
			RenderGO(go);
		}
	}

	//On screen text

	//Exercise 5: Render m_objectCount
	std::ostringstream ss1;
	ss1.precision(3);
	ss1 << "Active Object : " << m_objectCount;
	RenderTextOnScreen(meshList[GEO_TEXT], ss1.str(), Color(1, 1, 1), 3, 0, 9);
	ss1.str("");
	//Exercise 8c: Render initial and final momentum
	ss1.precision(5);
	ss1 << "Inital P: " << m1 * u1 + m2 * u2;
	RenderTextOnScreen(meshList[GEO_TEXT], ss1.str(), Color(1, 1, 1), 2, 0, 12);
	ss1.str("");

	ss1 << "Final P: " << m1 * v1 + m2 * v2;
	RenderTextOnScreen(meshList[GEO_TEXT], ss1.str(), Color(1, 1, 1), 2, 0, 15);
	ss1.str("");

	ss1.precision(3);
	ss1 << "Speed: " << m_speed;
	RenderTextOnScreen(meshList[GEO_TEXT], ss1.str(), Color(0, 1, 0), 3, 0, 6);
	ss1.str("");

	ss1.precision(5);
	ss1 << "FPS: " << fps;
	RenderTextOnScreen(meshList[GEO_TEXT], ss1.str(), Color(0, 1, 0), 3, 0, 3);
	ss1.str("");

	RenderTextOnScreen(meshList[GEO_TEXT], "Collision", Color(0, 1, 0), 3, 0, 0);
}

void SceneCollision::Exit()
{
	SceneBase::Exit();
	//Cleanup GameObjects
	while (m_goList.size() > 0)
	{
		GameObject *go = m_goList.back();
		delete go;
		m_goList.pop_back();
	}
	if (m_ghost)
	{
		delete m_ghost;
		m_ghost = NULL;
	}
}