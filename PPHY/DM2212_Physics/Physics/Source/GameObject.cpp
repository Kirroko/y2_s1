
#include "GameObject.h"

GameObject::GameObject(GAMEOBJECT_TYPE typeValue)
	: type(typeValue),
	scale(1, 1, 1),
	active(false),
	mass(1.f),
	angle(0.f),
	momentOfInertia(0),
	angularVelocity(0)
{
}

GameObject::~GameObject()
{
}