#pragma once
#include "BaseClass.h"

class DerivedClass : public BaseClass
{
private:
	int value;
	void SetValue(const int value);
public:
	DerivedClass();
	~DerivedClass();
};

