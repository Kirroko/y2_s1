#pragma once
#include <iostream>

using namespace std;

class APoint
{
private:
	int x, y;
public:
	APoint();
	APoint(int x, int y);
	~APoint();
	void Print();
	bool SetPos(const int x, const int y);
	
	APoint* WhoAmI(void);
	APoint operator+(APoint other);

	friend void someFriend(APoint&);
};

