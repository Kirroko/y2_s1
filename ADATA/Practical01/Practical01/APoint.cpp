#include "APoint.h"

APoint::APoint()
{
	x = 0;
	y = 0;
}

APoint::APoint(int x, int y)
{
	this->x = x;
	this->y = y;
}

APoint::~APoint()
{
}

void APoint::Print()
{
	cout << x << " " << y << endl;
}

bool APoint::SetPos(const int x, const int y)
{
	this->x = x;
	this->y = y;

	return false;
}

APoint * APoint::WhoAmI(void)
{
	return this;
}

APoint APoint::operator+(APoint other)
{
	APoint temp(0, 0);
	temp.x = this->x + other.x;
	temp.y = this->y + other.y;
	return temp;
}

void someFriend(APoint& obj)
{
	obj.x = 9;
	obj.y = 10;
}
