#include "APoint.h"

int main(void)
{
	APoint p1(1, 2);
	APoint p2(3, 4);
	APoint* ptr;

	// 1B
	ptr = p1.WhoAmI();
	ptr->Print();
	ptr = p2.WhoAmI();
	ptr->Print();

	p1.Print(); // 1A

	someFriend(p1);

	p1.Print();

	return 0;
}