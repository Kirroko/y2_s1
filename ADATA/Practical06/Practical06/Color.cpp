#include "Color.h"

SColor::SColor()
{
	r = g = b = 0;
}

void SColor::Set(int r, int g, int b)
{
	this->r = r;
	this->g = g;
	this->b = b;
}

void SColor::print()
{
	printf("%d %d %d\n", r, g, b);
}

bool SColor::operator>(const SColor & c)
{
	if (this->r > c.r) return true;
	else if (this->r == c.r)
		if (this->g > c.g) return true;
		else if (this->g == c.g)
			if (this->b > c.b) return true;

	return false;
}

SColor::~SColor()
{
}
