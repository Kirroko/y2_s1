#pragma once
#include <stdio.h>
#include <iostream>
#include "Color.h"

class Sorting
{
	void swap(int *a, int *b);
	void swap(SColor *a, SColor *b);
public:
	Sorting();
	void bubbleSort(int arr[], int n);
	void bubbleSort(SColor arr[], int n);
	void SelectionSort(int arr[], int n);
	void InsertionSort(int arr[], int n);
	void printArray(int arr[], int size);
	~Sorting();
};
