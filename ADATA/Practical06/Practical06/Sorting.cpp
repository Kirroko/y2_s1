#include "Sorting.h"

void Sorting::swap(int * a, int * b)
{
	int temp = *a; // hold first value
	*a = *b; // Replace first value with second value
	*b = temp; // Replace second value with first value
}

void Sorting::swap(SColor * a, SColor * b)
{
	SColor temp = *a;
	*a = *b; // Replace first value with second value
	*b = temp; // Replace second value with first value
}

Sorting::Sorting()
{
}

void Sorting::bubbleSort(int arr[], int n)
{
	int i, j;
	bool swapped;
	for (i = 0; i < n - 1; ++i)
	{
		swapped = false;
		for (j = 0; j < n - i - 1; ++j)
		{
			if (arr[j] > arr[j + 1])
			{
				swap(&arr[j], &arr[j + 1]);
				swapped = true;
			}
		}
		// If no two elements were swapped by inner loop, then break
		if (swapped == false)
			break;
	}
}

void Sorting::bubbleSort(SColor arr[], int n)
{
	int i, j;
	bool swapped;
	for (i = 0; i < n - 1; ++i)
	{
		swapped = false;
		for (j = 0; j < n - i - 1; ++j)
		{
			if (arr[j] > arr[j + 1])
			{
				swap(&arr[j], &arr[j + 1]);
				swapped = true;
			}
		}
		// If no two elements were swapped by inner loop, then break
		if (swapped == false)
			break;
	}
}

void Sorting::SelectionSort(int arr[], int n)
{
	int min_index;
	// move from start of index to the end
	for (int i = 0; i < n - 1; ++i)
	{
		// Find the minimum element in the unsorted array and move it the first element
		min_index = i;
		for (int j = i + 1; j < n; ++j)
			if (arr[j] < arr[min_index])
				min_index = j;

		// Swap the minimum element with the first element(that moves forward every iteration of the loop
		swap(&arr[min_index], &arr[i]);
	}
}

void Sorting::InsertionSort(int arr[], int n)
{
	int key, j;
	for (int i = 1; i < n; ++i)
	{
		key = arr[i];
		j = i - 1;
		/* Move elements of arr[0... i-1], that are
		greater than key, to one position ahead
		of their current position */
		while (j >= 0 && arr[j] > key)
		{
			arr[j + 1] = arr[j];
			j = j - 1;
		}
		arr[j + 1] = key;
	}
}

void Sorting::printArray(int arr[], int size)
{
	for (int i = 0; i < size; ++i)
		printf("%d ", arr[i]);
	printf("\n");
}

Sorting::~Sorting()
{
}