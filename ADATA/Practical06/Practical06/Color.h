#pragma once
#include <stdio.h>

//struct SColor
//{
//	int r, g, b;
//	void set(int r, int g, int b)
//	{
//		this->r = r;
//		this->g = g;
//		this->b = b;
//	}
//	void print()
//	{
//		printf("%d %d %d\n", r, g, b);
//	}
//};

class SColor
{
	int r, g, b;
public:
	SColor();
	void Set(int r, int g, int b);
	void print();
	bool operator>(const SColor& c);
	~SColor();
};