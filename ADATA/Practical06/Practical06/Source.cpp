#include "Sorting.h"
//#define _ADDITATION
#define _EXTRA
using namespace std;

void populateColors(SColor colors[])
{
	colors[0].Set(144, 33, 123);
	colors[1].Set(21, 134, 1);
	colors[2].Set(144, 234, 12);
	colors[3].Set(243, 123, 212);
	colors[4].Set(144, 33, 21);
	colors[5].Set(32, 123, 78);
	colors[6].Set(123, 21, 1);
	colors[7].Set(21, 231, 2);
}

void printArray(SColor arr[], int size)
{
	for (int i = 0; i < size; ++i)
		arr[i].print();
}

int main()
{
	int aList[10] = { 34,45,12,2,356,78,103,423,67,41 };
	int bList[10] = { 34,45,12,2,356,78,103,423,67,41 };
	int cList[10] = { 34,45,12,2,356,78,103,423,67,41 };
	int sizeOfList = (sizeof(aList) / sizeof(*aList));
	Sorting aSort;

#ifdef _ADDITATION
	// Bubble sort works by repeatedly swapping the adjacent elements if they are in wrong order.
	aSort.bubbleSort(aList, sizeOfList);
	aSort.printArray(aList, sizeOfList);
	// selection sort works by repeatedly finding the minimum element from unsorted part and putting it at the beginning.
	aSort.SelectionSort(bList, sizeOfList);
	aSort.printArray(bList, sizeOfList);
	// Insertion Sort works by repeatedly finding the sequence value and placing at the end of the found sequence
	aSort.InsertionSort(cList, sizeOfList);
	aSort.printArray(cList, sizeOfList);
#endif
#ifdef _EXTRA
	// Extra Exercise
	SColor colors[8];
	populateColors(colors);
	int n = (sizeof(colors) / sizeof(*colors));
	aSort.bubbleSort(colors, n);
	printArray(colors, n);
#endif

	return 0;
}