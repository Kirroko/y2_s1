#include "APoint.h"

APoint::APoint()
{
	x = 0;
	y = 0;
	z = 0;
	ptrZ = new int;
	*(this->ptrZ) = z;
}

APoint::APoint(int x, int y, int z)
{
	this->x = x;
	this->y = y;
	this->z = z;
	ptrZ = new int;
	*(this->ptrZ) = this->z;
}

APoint::~APoint()
{
}

void APoint::Print()
{
	cout << x << " " << y << " " << z << endl;
}

bool APoint::SetPos(const int x, const int y, const int z)
{
	this->x = x;
	this->y = y;
	this->z = z;
	/**this->ptrZ = z;*/
	return false;
}

APoint * APoint::WhoAmI(void)
{
	return this;
}

APoint APoint::operator+=(const APoint & rhs)
{
	x += rhs.x;
	y += rhs.y;
	*ptrZ += *rhs.ptrZ;
	return *this;
}

APoint & APoint::operator=(const APoint & rhs)
{
	x = rhs.x;
	y = rhs.y;
	*ptrZ = *rhs.ptrZ;

	return *this;
}

APoint APoint::operator++(void)
{
	++x;
	++y;
	++*ptrZ;
	return *this;
}

APoint APoint::operator++(int)
{
	APoint temp(*this);
	operator++(); // prefix-increment this instance
	return temp; // return value before increment
}

APoint APoint::operator--(void)
{
	--x;
	--y;
	--*ptrZ;
	return *this;
	return APoint();
}

APoint APoint::operator--(int)
{
	APoint temp(*this);
	operator--(); // prefix-decrement this instance
	return temp; // return value before increment
	return APoint();
}

APoint APoint::operator-(void)
{
	APoint temp;
	temp.x = -x;
	temp.y = -y;
	*temp.ptrZ = -*ptrZ;
	return temp;
}

bool APoint::operator!(void)
{
	if (x < 10 && y < 20 && *ptrZ < 30)
		return true;
	else
		return false;
}

APoint operator+(APoint & input1, APoint & input2)
{
	APoint temp;

	temp.x = input1.x + input2.x;
	temp.y = input1.y + input2.y;
	temp.z = input1.z + input2.z;

	return temp;
}

APoint operator+(APoint & p1, const int p2)
{
	APoint temp;
	temp.x = p1.x + p2;
	temp.y = p1.y + p2;
	temp.z = p1.z + p2;

	return temp;
}

APoint operator+(const int p1, APoint & p2)
{
	APoint temp;
	temp.x = p1 + p2.x;
	temp.y = p1 + p2.y;
	temp.z = p1 + p2.z;

	return temp;
}

APoint operator+(APoint & p1, const char p2)
{
	return APoint();
}

ostream & operator<<(ostream & os, const APoint & rhs)
{
	os << "[ " << rhs.x << ", " << rhs.y << ", " << *rhs.ptrZ << " ]";
	return os;
}

istream & operator>>(istream & is, APoint & rhs)
{
	is >> rhs.x >> rhs.y >> rhs.z;
	return is;
}



void someFriend(APoint& obj)
{
	obj.x = 9;
	obj.y = 10;
	obj.z = 11;
}
