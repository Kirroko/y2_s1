#include "APoint.h"

APoint::APoint()
{
	x = 0;
	y = 0;
	ptrZ = new int;
	*(this->ptrZ) = 0;
}

APoint::APoint(int x, int y)
{
	this->x = x;
	this->y = y;
	ptrZ = new int;
	*(this->ptrZ) = 0;
}

APoint::~APoint()
{
}

void APoint::Print()
{
	cout << x << " " << y << endl;
}

bool APoint::SetPos(const int x, const int y)
{
	this->x = x;
	this->y = y;
	*this->ptrZ = 888;
	return false;
}

APoint * APoint::WhoAmI(void)
{
	return this;
}

APoint APoint::operator+=(const APoint & rhs)
{
	x += rhs.x;
	y += rhs.y;
	return *this;
}

APoint & APoint::operator=(const APoint & rhs)
{
	x = rhs.x;
	y = rhs.y;
	*ptrZ = *rhs.ptrZ;

	return *this;
}

APoint operator+(APoint & input1, APoint & input2)
{
	APoint temp(0, 0);

	temp.x = input1.x + input2.x;
	temp.y = input1.y + input2.y;

	return temp;
}

APoint operator+(APoint & p1, const int p2)
{
	APoint temp(0, 0);
	temp.x = p1.x + p2;
	temp.y = p1.y + p2;

	return temp;
}

APoint operator+(const int p1, APoint & p2)
{
	APoint temp;
	temp.x = p1 + p2.x;
	temp.y = p1 + p2.y;

	return temp;
}

APoint operator+(APoint & p1, const char p2)
{
	return APoint();
}

ostream & operator<<(ostream & os, const APoint & rhs)
{
	os << "[ " << rhs.x << ", " << rhs.y << ", " << rhs.ptrZ << " ]";
	return os;
}

istream & operator>>(istream & is, APoint & rhs)
{
	is >> rhs.x >> rhs.y;
	return is;
}



void someFriend(APoint& obj)
{
	obj.x = 9;
	obj.y = 10;
}
