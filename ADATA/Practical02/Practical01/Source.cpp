#include "APoint.h"

int main(void)
{
	APoint p1(1, 2);
	APoint p2(3, 4);
	APoint p3(0, 0);

	//// 1B
	//ptr = p1.WhoAmI();
	//ptr->Print();
	//ptr = p2.WhoAmI();
	//ptr->Print();

	//p1.Print(); // 1A

	//someFriend(p1);

	//p1.Print();

	//P2 exercise 1A
	//p3 = p1 + p2;

	//P2 exercise 1B
	/*p1 = p2 + 2;
	p1 = 3 + p2;
	p2 = p1 + 'c';*/

	//P2 exercise 2
	/*cout << "Enter APoint values: ";
	cin >> p3;
	cout << p3;*/

	APoint thePoint1;
	thePoint1.SetPos(100, 200);

	APoint thePoint2;
	thePoint2 = thePoint1;

	cout << thePoint2;
	return 0;
}