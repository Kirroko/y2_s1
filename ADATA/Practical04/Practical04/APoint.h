#pragma once
#include <iostream>

using namespace std;

class APoint
{
private:
	int x, y, z;
	int* ptrZ;
public:
	APoint();
	APoint(int x, int y, int z);
	~APoint();
	void Print();
	void SetPos(const int x, const int y, const int z);
	void SetPos(const double x, const double y, const double z);
	void SetPos(const float x, const float y, const float z);
	void SetPos(const long x, const long y, const long z);
	void SetPos(const short x, const short y, const short z);

	
	APoint* WhoAmI(void);
	// APoint operator+(APoint other);
	APoint operator+=(const APoint& rhs);
	APoint& operator=(const APoint& rhs);
	APoint operator++(void); // pre
	APoint operator++(int); // post
	APoint operator--(void); // pre
	APoint operator--(int); // post
	APoint operator-(void); // unary
	bool operator!(void);

	friend APoint operator+(APoint & input1, APoint & input2); // The default overloading operator accept one parameter (this, 2nd value). Make a friend takes away the "this" in the parameter (op1,op2)
	friend APoint operator+(APoint& p1, const int p2);
	friend APoint operator+(const int p1, APoint& p2);
	friend APoint operator+(APoint& p1, const char p2);

	friend ostream& operator<<(ostream& os, const APoint& rhs);
	friend istream& operator>>(istream& is, APoint& rhs);



	friend void someFriend(APoint&);
};

