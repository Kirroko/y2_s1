#pragma once

template <class ph>
class CStorage
{
	ph a, b;
public:
	CStorage(ph a, ph b);
	~CStorage();
	ph GetA();
	ph GetB();
};

