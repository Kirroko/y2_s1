#include "CStorage.h"

template <class ph>
CStorage<ph>::CStorage(ph a, ph b)
{
	this->a = a;
	this->b = b;
}

template<class ph>
CStorage<ph>::~CStorage()
{
}

template<class ph>
ph CStorage<ph>::GetA()
{
	return this->a;
}

template<class ph>
ph CStorage<ph>::GetB()
{
	return this->b;
}
