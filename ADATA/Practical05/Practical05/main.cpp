#include <iostream>

using namespace std;

void SeqentialSearch(int value[], int num)
{
	bool found = false;
	for (int i = 0; i < 11; ++i)
	{
		if (value[i] == num)
		{
			cout << num << " is at index " << i << endl;
			found = true;
			break;
		}
	}
	if (!found)
		cout << "Number not found" << endl;
}

/*
	A recursive binary search function. It returns
	location of x in given array arr[1...high] is present,
	otherwise -1
*/
int binarySearch(int arr[], int low, int high, int value)
{
	if (high >= low)
	{
		int mid = low + (high - low) / 2;

		// If the element is present at the middle
		// itself
		if (arr[mid] == value)
			return mid;

		// If element is smaller than mid, then
		// it can only be present in left subarray
		if (arr[mid] > value)
			return binarySearch(arr, low, mid - 1, value);

		return binarySearch(arr, mid + 1, high, value);
	}

	// We reach here when element is not
	// present in array
	return -1;
}

bool isSorted(int arr[], int size)
{
	// ascending
	for (int i = 1; i < size; i++)
	{
		int cmp = (arr[i] - arr[i - 1]) * 1;
		if (cmp > 0) return false;
	}
	// descending
	for (int i = 1; i < size; ++i)
	{
		int cmp = (arr[i] - arr[i - 1]) * -1;
		if (cmp > 0) return false;
	}
	return true;
}

int main()
{
	int NUMBER = 0;
	int list[] = { 1,2,8,15,99,111,200,199,210,1010,1500 };

	cout << "What number are you searching for : " << endl;
	cin >> NUMBER;
	
	// SeqentialSearch(list, NUMBER);

	int n = sizeof(list) / sizeof(list[0]);
	/*int result = binarySearch(list, 0, n - 1, NUMBER);
	(result == -1) ? cout << "Number not found" : cout << "Element is present at index " << result << endl;*/

	if (isSorted(list, n))
	{
		int result = binarySearch(list, 0, n - 1, NUMBER);
		(result == -1) ? cout << "Number not found" : cout << "Element is present at index " << result << endl;
	}
	else
	{
		SeqentialSearch(list, NUMBER);
	}

	return 0;
}