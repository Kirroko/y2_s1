#include "Expression.h"
//#define LOOP_TEST
#define SINGLE_TEST
using namespace std;

void UI(void)
{
	cout << "**********************" << endl;
	cout << "* Fraction Calculator*" << endl;
	cout << "**********************" << endl;
	cout << "(1) Arithmetic Expression" << endl;
	cout << "(2) Simplification" << endl;
	cout << "Your choice: ";
}

int main()
{
	Expression AExpress;

#ifdef LOOP_TEST
	while (1)
	{
		UI();
		int userChoice = 0;
		cin >> userChoice;
		cout << endl;
		AExpress.GetInput(userChoice);
		cout << endl;
	}
#endif // LOOP_TEST
#ifdef SINGLE_TEST
	UI();
	int userChoice = 0;
	cin >> userChoice;
	cout << endl;
	AExpress.GetInput(userChoice);
	cout << endl;
#endif // SINGLE_TEST

	return 0;
}