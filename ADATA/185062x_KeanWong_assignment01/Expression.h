#pragma once
#include "Fraction.h"
#include <string>
#include <vector>
class Expression
{
	Fraction operand1, operand2;
	char optr;
	void ProcessInput(std::string input);
	void ProcessInput2(std::string input);
	Fraction Calculation(void);
	bool bCalculation(void);
public:
	Expression();
	void GetInput(int mode);
	~Expression();
};

