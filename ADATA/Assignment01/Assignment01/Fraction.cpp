#include "Fraction.h"

bool Fraction::Simplify()
{
	bool sucessful = false;
	for (int i = Math::Min(numerator, denominator); i; i--)
	{
		if ((numerator % i == 0) && (denominator % i == 0))
		{
			numerator /= i;
			denominator /= i;
			sucessful = true;
		}
	}
	if (denominator == 1)
	{
		wholeNumber = numerator;
		numerator = 0;
	}
	else if (numerator == denominator)
	{
		wholeNumber += 1;
		numerator = 0;
	}
	else if (numerator > denominator)
	{
		wholeNumber += numerator / denominator;
		numerator = numerator % denominator;
	}
	return sucessful;
}

int Fraction::lcm(int a, int b)
{
	return (a * b) / gcd(a,b);
}

int Fraction::gcd(int a, int b)
{
	if (a == 0)
		return b;
	return gcd(b % a, a); // recursive function to find the greatest common division
}

Fraction::Fraction()
	: wholeNumber(0)
	, numerator(0)
	, denominator(1) // you can't have zero because you'll be dividing by zero.
{
}

Fraction::Fraction(int w)
{
	wholeNumber = w;
	numerator = 0;
	denominator = 1;
}

Fraction::Fraction(int n, int d)
{
	numerator = n;
	denominator = d;
	// safety first to prevent divide by zero from user [Even if assume just in case]
	if (denominator == 0)
	{
		numerator = 0;
		denominator = 1;
		std::cout << "Math ERROR" << std::endl;
		std::cin.ignore();
	}
	// Auto convert fraction to mix fraction?
	// if numerator > denominator | true : false
	wholeNumber = numerator > denominator ? numerator / denominator : 0;
	// if wholeNumber != 0 | true : false;
	wholeNumber != 0 ? numerator -= (numerator / denominator) * denominator : 0; // Rmb the DATA type is in int so division with left over is floored
	Simplify();
}

Fraction::Fraction(int w, int n, int d)
{
	numerator = n;
	denominator = d;
	// safety first to prevent divide by zero from user [Even if assume just in case]
	if (denominator == 0)
	{
		numerator = 0;
		denominator = 1;
		std::cout << "Math ERROR" << std::endl;
		std::cin.ignore();
	}
	// Auto convert fraction to mix fraction?
	// if numerator > denominator | true : false
	wholeNumber = numerator > denominator ? numerator / denominator : 0 + wholeNumber;
	// if wholeNumber != 0 | true : false;
	wholeNumber != 0 ? numerator -= (numerator / denominator) * denominator : 0; // Rmb the DATA type is in int so division with left over is floored
	Simplify();
}

Fraction::~Fraction()
{
}

void Fraction::setWholeNumber(int w)
{
	wholeNumber = w;
}

void Fraction::setNumerator(int n)
{
	numerator = n;
}

void Fraction::setDenominator(int d)
{
	denominator = d;
}

void Fraction::setFraction(int w, int n, int d)
{
	numerator = n;
	denominator = d;
	// safety first to prevent divide by zero from user [Even if assume just in case]
	if (denominator == 0)
	{
		numerator = 0;
		denominator = 1;
		std::cout << "Math ERROR" << std::endl;
		std::cin.ignore();
	}
	// Auto convert fraction to mix fraction?
	// if numerator > denominator | true : false
	wholeNumber = numerator > denominator ? numerator / denominator : 0 + wholeNumber;
	// if wholeNumber != 0 | true : false;
	wholeNumber != 0 ? numerator -= (numerator / denominator) * denominator : 0; // Rmb the DATA type is in int so division with left over is floored
	Simplify();
}

void Fraction::setDefault(void)
{
	wholeNumber = 0;
	numerator = 0;
	denominator = 1;
}

void Fraction::print()
{
	if (numerator != 0)
		if (wholeNumber != 0)
			std::cout << wholeNumber << 'a' << numerator << 'o' << denominator;
		else
			std::cout << numerator << 'o' << denominator;
	else
		std::cout << wholeNumber << std::endl;
	// should write anything if numerator is 0 though?
}

Fraction Fraction::operator+(const Fraction & rhs) const
{
	// !!factor in the whole number!!
	int tempNumerator = numerator + (wholeNumber * denominator);
	int tempRHSNumerator = rhs.numerator + (rhs.wholeNumber * rhs.denominator);
	// Rmb to use changed value
	int matchedDenominator = denominator * rhs.denominator;
	Fraction temp(
		((matchedDenominator / denominator) * tempNumerator) +
		((matchedDenominator / rhs.denominator) * tempRHSNumerator)
		, matchedDenominator);
	temp.Simplify();
	return temp;
}

Fraction Fraction::operator-(const Fraction & rhs) const
{
	// !!factor in the whole number!!
	int tempNumerator = numerator + (wholeNumber * denominator);
	int tempRHSDenominator = rhs.denominator + (rhs.wholeNumber * rhs.denominator);
	// Rmb to use changed value
	int matchedDenominator = denominator * tempRHSDenominator;
	Fraction temp(
		((matchedDenominator / denominator) * tempNumerator) -
		((matchedDenominator / tempRHSDenominator) * rhs.numerator)
		, matchedDenominator);
	temp.Simplify();
	return temp;
}

Fraction Fraction::operator*(const Fraction & rhs) const
{
	int tempNumerator = numerator + (wholeNumber * denominator);
	int tempRHSNumerator = rhs.numerator + (rhs.wholeNumber * rhs.denominator);
	Fraction temp(tempNumerator * tempRHSNumerator, denominator * rhs.denominator);
	temp.Simplify();
	return temp;
}

Fraction Fraction::operator/(const Fraction & rhs) const
{
	int tempNumerator = numerator + (wholeNumber * denominator);
	int tempRHSNumerator = rhs.numerator + (rhs.wholeNumber * rhs.denominator);
	Fraction temp(tempNumerator * rhs.denominator, denominator * tempRHSNumerator);
	temp.Simplify();
	return temp;
}

Fraction & Fraction::operator=(const Fraction & rhs) 
{
	wholeNumber = rhs.wholeNumber;
	numerator = rhs.numerator;
	denominator = rhs.denominator;
	return *this;
}

bool operator==(const Fraction & lhs, const Fraction & rhs)
{
	if (lhs.wholeNumber != rhs.wholeNumber)
		return false;
	// 1/6 | 1/12
	int lcd = Fraction::lcm(lhs.denominator, rhs.denominator);
		
	int lhsNumerator = lhs.numerator;
	int rhsNumerator = rhs.numerator;

	lhsNumerator *= (lcd / lhs.denominator);
	rhsNumerator *= (lcd / rhs.denominator);

	if (lhsNumerator == rhsNumerator)
		return true;
	else
		return false;
}

bool operator<(const Fraction & lhs, const Fraction & rhs)
{
	if (lhs.wholeNumber < rhs.wholeNumber)
		return true;
	
	int lcd = Fraction::lcm(lhs.denominator, rhs.denominator);

	int lhsNumerator = lhs.numerator;
	int rhsNumerator = rhs.numerator;

	lhsNumerator *= (lcd / lhs.denominator);
	rhsNumerator *= (lcd / rhs.denominator);

	if (lhsNumerator < rhsNumerator)
		return true;
	else
		return false;
}

bool operator>(const Fraction & lhs, const Fraction & rhs)
{
	if (lhs.wholeNumber > rhs.wholeNumber)
		return true;

	int lcd = Fraction::lcm(lhs.denominator, rhs.denominator);

	int lhsNumerator = lhs.numerator;
	int rhsNumerator = rhs.numerator;

	lhsNumerator *= (lcd / lhs.denominator);
	rhsNumerator *= (lcd / rhs.denominator);

	if (lhsNumerator > rhsNumerator)
		return true;
	else
		return false;
}

std::ostream & operator<<(std::ostream & out, Fraction & rhs)
{
	rhs.print();
	return out;
}

std::istream & operator>>(std::istream & in, Fraction & rhs)
{
	int w, n, d;
	std::cout << "Please enter WHOLE NUMBER [Space] NUMERATOR [Space] DENOMINATOR" << std::endl;
	in >> w >> n >> d;
	rhs.setFraction(w, n, d);
	return in;
}
