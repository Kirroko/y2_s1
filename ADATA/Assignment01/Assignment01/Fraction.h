#pragma once
#include <iostream>
#include "MyMath.h"

class Fraction
{
	int wholeNumber, numerator, denominator;
	// lowest common multiple
	static int lcm(int a, int b);
	// greatest common division
	static int gcd(int a, int b);
public:
	Fraction();
	Fraction(int w);
	Fraction(int n, int d);
	Fraction(int w, int n, int d);
	~Fraction();
	
	// Equivalent shrinker or simplify function
	bool Simplify();

	void setWholeNumber(int w);
	void setNumerator(int n);
	void setDenominator(int d);
	void setFraction(int w, int n, int d);
	void setDefault(void);
	void print();

	Fraction operator+(const Fraction& rhs) const; // Fraction addition
	Fraction operator-(const Fraction& rhs) const; // Fraction subtraction
	Fraction operator*(const Fraction& rhs) const; // Fraction multiplication
	Fraction operator/(const Fraction& rhs) const; // Fraction division
	Fraction& operator=(const Fraction& rhs);		// Fraction Assignment

	friend bool operator==(const Fraction& lhs, const Fraction& rhs);
	friend bool operator<(const Fraction& lhs, const Fraction& rhs);
	friend bool operator>(const Fraction& lhs, const Fraction& rhs);
	friend std::ostream& operator<<(std::ostream& out, Fraction &rhs); // print to ostream
	friend std::istream& operator>>(std::istream& in, Fraction &rhs); // intake from istream
};
