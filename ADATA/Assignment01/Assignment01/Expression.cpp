#include "Expression.h"

Expression::Expression()
{
}

void Expression::GetInput(int mode)
{
	operand1.setDefault();
	operand2.setDefault();
	std::string userInput;
	if (mode == 1)
		std::cout << "< Arithmetic Expression >" << std::endl;
	else
		std::cout << "< Simplification >" << std::endl;
	std::cout << "Enter expression: ";
	std::cin >> userInput;
	if (mode == 1)
		ProcessInput(userInput);
	else if(mode == 2)
		ProcessInput2(userInput);
	Fraction temp;
	bool result = false;
	if (optr == '+' || optr == '-' || optr == '*' || optr == '/')
	{
		temp = Calculation();
		std::cout << "Result: " << temp << std::endl;
	}
	else if(optr == '<' || optr == '>' || optr == '=')
	{
		result = bCalculation();
		if (result)
			std::cout << "Result: true" << std::endl;
		else
			std::cout << "Result: false" << std::endl;
		
	}
	else
	{
		std::cout << "Result: " << operand1 << std::endl;
	}
}

void Expression::ProcessInput(std::string input)
{
	static std::string temp = "";
	static bool op = 0;
	for (std::string::size_type i = 0; i < input.size(); ++i)
	{
		switch (input[i])
		{
		case '+':
			optr = '+';
			if (op == 0)
			{
				std::string innerTemp = "";
				bool isFraction = false;
				for (std::string::size_type i = 0; i < temp.size(); ++i)
				{
					switch (temp[i])
					{
					case 'a':
						operand1.setWholeNumber(std::stoi(innerTemp));
						isFraction = true;
						innerTemp.clear();
						break;
					case 'o':
						operand1.setNumerator(std::stoi(innerTemp));
						isFraction = true;
						innerTemp.clear();
						break;
					default:
						innerTemp.push_back(temp[i]);
						break;
					}
				}
				if (isFraction)
				{
					op = true;
					temp.clear();
					operand1.setDenominator(std::stoi(innerTemp));
				}
				else
				{
					op = true;
					temp.clear();
					operand1.setWholeNumber(std::stoi(innerTemp));
				}
			}
			break;
		case '-':
			optr = '-';
			if (op == 0)
			{
				std::string innerTemp = "";
				bool isFraction = false;
				for (std::string::size_type i = 0; i < temp.size(); ++i)
				{
					switch (temp[i])
					{
					case 'a':
						operand1.setWholeNumber(std::stoi(innerTemp));
						isFraction = true;
						innerTemp.clear();
						break;
					case 'o':
						operand1.setNumerator(std::stoi(innerTemp));
						isFraction = true;
						innerTemp.clear();
						break;
					default:
						innerTemp.push_back(temp[i]);
						break;
					}
				}
				if (isFraction)
				{
					op = true;
					temp.clear();
					operand1.setDenominator(std::stoi(innerTemp));
				}
				else
				{
					op = true;
					temp.clear();
					operand1.setWholeNumber(std::stoi(innerTemp));
				}
			}

			break;
		case '/':
			optr = '/';
			if (op == 0)
			{
				std::string innerTemp = "";
				bool isFraction = false;
				for (std::string::size_type i = 0; i < temp.size(); ++i)
				{
					switch (temp[i])
					{
					case 'a':
						operand1.setWholeNumber(std::stoi(innerTemp));
						isFraction = true;
						innerTemp.clear();
						break;
					case 'o':
						operand1.setNumerator(std::stoi(innerTemp));
						isFraction = true;
						innerTemp.clear();
						break;
					default:
						innerTemp.push_back(temp[i]);
						break;
					}
				}
				if (isFraction)
				{
					op = true;
					temp.clear();
					operand1.setDenominator(std::stoi(innerTemp));
				}
				else
				{
					op = true;
					temp.clear();
					operand1.setWholeNumber(std::stoi(innerTemp));
				}
			}

			break;
		case '*':
			optr = '*';
			if (op == 0)
			{
				std::string innerTemp = "";
				bool isFraction = false;
				for (std::string::size_type i = 0; i < temp.size(); ++i)
				{
					switch (temp[i])
					{
					case 'a':
						operand1.setWholeNumber(std::stoi(innerTemp));
						isFraction = true;
						innerTemp.clear();
						break;
					case 'o':
						operand1.setNumerator(std::stoi(innerTemp));
						isFraction = true;
						innerTemp.clear();
						break;
					default:
						innerTemp.push_back(temp[i]);
						break;
					}
				}
				if (isFraction)
				{
					op = true;
					temp.clear();
					operand1.setDenominator(std::stoi(innerTemp));
				}
				else
				{
					op = true;
					temp.clear();
					operand1.setWholeNumber(std::stoi(innerTemp));
				}
			}

			break;
		case '<':
			optr = '<';
			if (op == 0)
			{
				std::string innerTemp = "";
				bool isFraction = false;
				for (std::string::size_type i = 0; i < temp.size(); ++i)
				{
					switch (temp[i])
					{
					case 'a':
						operand1.setWholeNumber(std::stoi(innerTemp));
						isFraction = true;
						innerTemp.clear();
						break;
					case 'o':
						operand1.setNumerator(std::stoi(innerTemp));
						isFraction = true;
						innerTemp.clear();
						break;
					default:
						innerTemp.push_back(temp[i]);
						break;
					}
				}
				if (isFraction)
				{
					op = true;
					temp.clear();
					operand1.setDenominator(std::stoi(innerTemp));
				}
				else
				{
					op = true;
					temp.clear();
					operand1.setWholeNumber(std::stoi(innerTemp));
				}
			}

			break;
		case '>':
			optr = '>';
			if (op == 0)
			{
				std::string innerTemp = "";
				bool isFraction = false;
				for (std::string::size_type i = 0; i < temp.size(); ++i)
				{
					switch (temp[i])
					{
					case 'a':
						operand1.setWholeNumber(std::stoi(innerTemp));
						isFraction = true;
						innerTemp.clear();
						break;
					case 'o':
						operand1.setNumerator(std::stoi(innerTemp));
						isFraction = true;
						innerTemp.clear();
						break;
					default:
						innerTemp.push_back(temp[i]);
						break;
					}
				}
				if (isFraction)
				{
					op = true;
					temp.clear();
					operand1.setDenominator(std::stoi(innerTemp));
				}
				else
				{
					op = true;
					temp.clear();
					operand1.setWholeNumber(std::stoi(innerTemp));
				}
			}

			break;
		case '=':
			optr = '=';
			++i;
			if (op == 0)
			{
				std::string innerTemp = "";
				bool isFraction = false;
				for (std::string::size_type i = 0; i < temp.size(); ++i)
				{
					switch (temp[i])
					{
					case 'a':
						operand1.setWholeNumber(std::stoi(innerTemp));
						isFraction = true;
						innerTemp.clear();
						break;
					case 'o':
						operand1.setNumerator(std::stoi(innerTemp));
						isFraction = true;
						innerTemp.clear();
						break;
					default:
						innerTemp.push_back(temp[i]);
						break;
					}
				}
				if (isFraction)
				{
					op = true;
					temp.clear();
					operand1.setDenominator(std::stoi(innerTemp));
				}
				else
				{
					op = true;
					temp.clear();
					operand1.setWholeNumber(std::stoi(innerTemp));
				}
			}
		default:
			temp.push_back(input[i]);
			break;
		}
	}
	if (op)
	{
		std::string innerTemp = "";
		bool isFraction = false;
		for (std::string::size_type i = 0; i < temp.size(); ++i)
		{
			switch (temp[i])
			{
			case 'a':
				operand2.setWholeNumber(std::stoi(innerTemp));
				isFraction = true;
				innerTemp.clear();
				break;
			case 'o':
				operand2.setNumerator(std::stoi(innerTemp));
				isFraction = true;
				innerTemp.clear();
				break;
			default:
				innerTemp.push_back(temp[i]);
				break;
			}
		}
		if (isFraction)
		{
			op = true;
			temp.clear();
			operand2.setDenominator(std::stoi(innerTemp));
		}
		else
		{
			op = true;
			temp.clear();
			operand2.setWholeNumber(std::stoi(innerTemp));
		}
	}
}

void Expression::ProcessInput2(std::string input)
{
	static std::string temp = "";
	for (std::string::size_type i = 0; i < input.size(); ++i)
	{
		switch (input[i])
		{
		case 'a':
			operand1.setWholeNumber(std::stoi(temp));
			temp.clear();
			break;
		case 'o':
			operand1.setNumerator(std::stoi(temp));
			temp.clear();
			break;
		default:
			temp.push_back(input[i]);
			break;
		}
	}
	operand1.setDenominator(std::stoi(temp));
	operand1.Simplify();
	temp.clear();
}

Fraction Expression::Calculation(void)
{
	switch (optr)
	{
	case '+':
		return operand1 + operand2;
	case '-':
		return operand1 - operand2;
	case '/':
		return operand1 / operand2;
	case '*':
		return operand1 * operand2;
	default:
		std::cout << "Switch case error, Expression.cpp opt" << std::endl;
		break;
	}
}

bool Expression::bCalculation(void)
{
	switch (optr)
	{
	case '<':
		return operand1 < operand2;
	case '>':
		return operand1 > operand2;
	case '=':
		return operand1 == operand2;
	default:
		std::cout << "switch error, Expression.cpp Boolean" << std::endl;
		break;
	}
	return false;
}

Expression::~Expression()
{
}