WASD - move forward, left, backward, right
Arrow keys - pitch, yaw camera
IJKLOP - light movement
1,2 - toggle front face culling
3,4 - toggle wireframe mode
5,6,7 - toggle point, directional, spotlight

8 - Day time
9 - Night time
F - Disable/Enable Fog

Feature:
-Multitexturing (Ground, Rocks)
-Fog
-Sprite Animation
-Glowing Light
-Skyplane
-Terrain normal
